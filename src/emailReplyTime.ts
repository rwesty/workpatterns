import { Email, EmailReplyMap, DomainMap } from './types';

/**
 * Given a list of emails, return the reply and response pairs by email domain.
 * For each email:
 *  1. Store its value in a map indexed by 'message-id'.
 *  2. If the email is in reply to another, store its value in a map indexed by 'in-reply-to'
 *  3. If the `from` and `to` field of the indexed message is known, save the pair in a map indexed by domain name
 *  4. If the `from` and `to` field of the replied message is known, save the pair in a map indexed by domain name
 *  5. Return the map of domain names
 */
const emailReplyTimes = (emails: Email[]) => {
    const emailMap: EmailReplyMap = {};
    const domainMap: DomainMap = {};

    for (let email of emails) {
        const newRecord = {
            domain: extractSenderDomain(email),
            time: email.time,
        };

        emailMap[email['message-id']] = {
            ...emailMap[email['message-id']],
            message: newRecord,
        };
        const record = emailMap[email['message-id']];

        if (email['in-reply-to']) {
            emailMap[email['in-reply-to']] = {
                ...emailMap[email['in-reply-to']],
                reply: newRecord,
            };
        }
        const inReplyRecord = email['in-reply-to']
            ? emailMap[email['in-reply-to']]
            : false;

        if (
            record.message &&
            record.reply &&
            record.reply.domain !== 'workpatterns.ai'
        ) {
            const messageId = email['message-id'];

            domainMap[record.reply.domain] = {
                ...domainMap[record.reply.domain],
                [messageId]: {
                    timestamp: record.reply.time,
                    timeToReply: record.reply.time - record.message.time,
                },
            };
        }

        if (
            inReplyRecord &&
            email['in-reply-to'] &&
            inReplyRecord.message &&
            inReplyRecord.reply &&
            inReplyRecord.reply.domain !== 'workpatterns.ai'
        ) {
            const replyId = email['in-reply-to'];
            domainMap[inReplyRecord.reply.domain] = {
                ...domainMap[inReplyRecord.reply.domain],
                [replyId]: {
                    timestamp: inReplyRecord.reply.time,
                    timeToReply:
                        inReplyRecord.reply.time - inReplyRecord.message.time,
                },
            };
        }
    }

    return domainMap;
};

const extractSenderDomain = (email: Email) => {
    return email.addresses
        .find((a) => a.role === 'sender')! // Sender is guranteed in provided data.
        .address!.split('@')[1];
};

export default emailReplyTimes;

import React, { useEffect, useState } from 'react';
import styled from 'styled-components/macro';

import { DomainMap } from './types';
import { Aggregate, GroupBy, transformReplyTimesToDataTable } from './utils';
import emailReplyTimes from './emailReplyTime';

import { Column, Data, DataTable } from './DataTable';

import emails from './emails.json';

function App() {
    const [replyTimesByDomain, setReplyTimeByDomain] = useState<DomainMap>();
    const [groupBy, setGroupBy] = useState<GroupBy>('month');
    const [aggregate, setAggregate] = useState<Aggregate>('average');
    const [columns, setColumns] = useState<Column[]>();
    const [data, setData] = useState<Data[]>();

    useEffect(() => {
        setReplyTimeByDomain(emailReplyTimes(emails));
    }, []);

    useEffect(() => {
        if (replyTimesByDomain) {
            const [columns, data] = transformReplyTimesToDataTable(
                replyTimesByDomain,
                {
                    groupBy,
                    aggregate,
                },
            );
            setColumns(columns);
            setData(data);
        }
    }, [replyTimesByDomain, groupBy, aggregate]);

    return (
        <>
            <Navbar>
                <Logo src={`${process.env.PUBLIC_URL}/wp-logo.png`} />
            </Navbar>
            <Wrapper>
                {columns && data && (
                    <DataTable
                        title={
                            <>
                                Dmitri's Emails as{' '}
                                <DropdownWrapper>
                                    <Dropdown
                                        name="aggregate"
                                        defaultValue="average"
                                        onChange={(e) =>
                                            setAggregate(
                                                e.target.value as Aggregate,
                                            )
                                        }
                                    >
                                        <option value="average">
                                            Average Reply Time
                                        </option>
                                        <option value="count">
                                            Count of Replies
                                        </option>
                                    </Dropdown>
                                </DropdownWrapper>{' '}
                                per{' '}
                                <DropdownWrapper>
                                    <Dropdown
                                        name="groupBy"
                                        defaultValue="month"
                                        onChange={(e) =>
                                            setGroupBy(
                                                e.target.value as GroupBy,
                                            )
                                        }
                                    >
                                        <option value="month">Month</option>
                                        <option value="dayOfWeek">
                                            Day of Week
                                        </option>
                                    </Dropdown>
                                </DropdownWrapper>
                            </>
                        }
                        columns={columns}
                        data={data}
                    />
                )}
            </Wrapper>
        </>
    );
}

const Navbar = styled.div`
    height: 40px;
    background: #e6e6e6;
    padding: 20px 111.5px;
`;

const Logo = styled.img`
    height: 100%;
`;

const Wrapper = styled.div`
    padding: 15px 111.5px;
`;

const DropdownWrapper = styled.span`
    display: inline-block;
    position: relative;

    ::after {
        content: '';
        background: url(${process.env.PUBLIC_URL}/caret.svg) center center/cover;
        position: absolute;
        height: 8px;
        width: 8px;
        right: 8px;
        top: 50%;
        transform: translateY(-50%);
    }
`;

const Dropdown = styled.select`
    cursor: pointer;
    border: 1px solid #d5d8df;
    border-radius: 5px;
    padding: 4px 24px 4px 14px;
    font-size: 18px;
    appearance: none;
    font-family: 'Nunito';
    background: #0238bb;
    color: white;

    :hover {
        background: #ad38bb;
    }
`;

export default App;

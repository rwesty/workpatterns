type Address = {
    role: 'sender' | 'receiver';
    address: string;
};

export type Email = {
    'message-id': string;
    time: number;
    'in-reply-to'?: string;
    addresses: Address[];
};

export type ReplyRecord = {
    domain: string;
    time: number;
};

type ReplyPair = {
    message: ReplyRecord;
    reply: ReplyRecord;
};

export type EmailReplyMap = Record<string, ReplyPair>;

export type DomainRecord = {
    timestamp: number;
    timeToReply: number;
};

export type DomainMap = Record<string, Record<string, DomainRecord>>;

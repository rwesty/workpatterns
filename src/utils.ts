import { format } from 'date-fns';
import humanize from 'humanize-duration';

import { Column, Data } from './DataTable';
import { DomainMap, DomainRecord } from './types';

export type GroupBy = 'month' | 'dayOfWeek';
export type Aggregate = 'average' | 'count';

type TransformOptions = {
    groupBy: GroupBy;
    aggregate: Aggregate;
};

export const transformReplyTimesToDataTable = (
    replyTimes: DomainMap,
    options: TransformOptions,
): [Column[], Data[]] => {
    const columns: Record<string, Column> = {
        org: {
            name: 'Organization',
            selector: 'org',
            width: '240px',
            sortable: true,
        },
    };
    const data: Data[] = [];
    const allOrgs: Data = {
        id: data.length,
        org: 'All Organizations',
    };

    for (let [domain, domainReplies] of Object.entries(replyTimes)) {
        const item = Object.values(domainReplies).reduce((dataRow, reply) => {
            const headerTitle = groupedByHeaderTitle(options.groupBy, reply);
            const headerSelector = groupedByHeaderSelector(
                options.groupBy,
                reply,
            );
            columns[headerTitle] = {
                name: headerTitle,
                selector: headerSelector,
                sortable: true,
                format: (row) =>
                    prettyValue(options.aggregate, row[headerSelector]),
            };
            dataRow[headerSelector] = aggregateFunction(
                options.aggregate,
                dataRow[headerTitle],
                reply.timeToReply,
            );
            return dataRow;
        }, {} as Record<string, number>);

        for (let [key, val] of Object.entries(item)) {
            allOrgs[key] = aggregateFunction(
                options.aggregate,
                allOrgs[key],
                val,
            );
        }

        data.push({
            id: data.length + 1,
            org: domain,
            ...item,
        });
    }

    return [Object.values(columns), [allOrgs, ...data]];
};

const groupedByHeaderTitle = (groupBy: GroupBy, reply: DomainRecord) => {
    if (groupBy === 'month') {
        return format(new Date(reply.timestamp * 1000), "MMM yy'''");
    } else if (groupBy === 'dayOfWeek') {
        return format(new Date(reply.timestamp * 1000), 'eeee');
    }

    throw new Error('groupBy must be "month" or "dayOfWeek"');
};

const groupedByHeaderSelector = (groupBy: GroupBy, reply: DomainRecord) => {
    if (groupBy === 'month') {
        return format(new Date(reply.timestamp * 1000), 'yyyy-MM');
    } else if (groupBy === 'dayOfWeek') {
        return format(new Date(reply.timestamp * 1000), 'eeee');
    }

    throw new Error('groupBy must be "month" or "dayOfWeek"');
};

const aggregateFunction = (
    aggregate: Aggregate,
    existingValue: number,
    newValue: number,
) => {
    if (aggregate === 'average') {
        // When there is an existing value, adjust the average
        return existingValue ? (existingValue + newValue) / 2 : newValue;
    } else if (aggregate === 'count') {
        return existingValue ? existingValue + 1 : 1;
    }

    throw new Error('aggregate must be "average" or "count"');
};

const prettyValue = (aggregate: Aggregate, value: number) => {
    if (value === undefined) {
        return '-';
    }

    if (aggregate === 'average') {
        return humanize(Math.floor(value * 1000), { largest: 1 });
    }

    return value;
};

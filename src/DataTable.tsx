import * as React from 'react';
import styled from 'styled-components/macro';
import ReactDataTable, {
    TableProps,
    TableColumn,
} from 'react-data-table-component';

export type Column = TableColumn;
export type Data = {
    [header: string]: any;
};

type Props = TableProps<Data>;

export function DataTable({ columns, data, ...rest }: Props) {
    return (
        <StyledDataTable
            columns={columns}
            data={data}
            defaultSortField="org"
            defaultSortAsc={true}
            responsive
            {...rest}
        />
    );
}

// Fix: React data table doesn't play nice with the applied styled components classes
const StyledDataTable = styled(({ className, ...rest }) => (
    <div className={className}>
        <ReactDataTable {...rest} />
    </div>
))`
    border: 1px solid #d5d8df;
    border-radius: 5px;
    padding: 20px;

    .rdt_TableRow {
        font-size: 16px;
    }

    .rdt_TableHeader {
        min-height: 0;
    }

    .rdt_TableHeader > * {
        font-weight: bold;
    }

    .rdt_TableCol {
        font-weight: bold;
        font-size: 14px;
        color: #676d7d;
    }
`;
